import React from "react";
import { Table } from "../components";
import {useSelector} from "react-redux";
import {IAppState} from "../store";

const Homepage = () => {
    // const items = useSelector((state: IAppState) => state.itemsState.items);
    let totalPrice: number = useSelector((state: IAppState) => state.itemsState.totalPrice);

    return (
        <div className="container">
            <Table /><br/>
            <div>
                Total Price: {totalPrice}
            </div>
        </div>
    )
}

export default Homepage;