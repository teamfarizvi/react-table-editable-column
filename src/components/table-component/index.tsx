import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { IAppState } from "../../store";
import { getItemsAction, loadItemsAction } from "../../app/redux/actions/itemActions";
import TableRow from "../table-row";

const Table = () => {
    const dispatch = useDispatch();
    const items = useSelector((state: IAppState) => state.itemsState.items);

    const getItems = useCallback(() => {
        dispatch(loadItemsAction(true));
        dispatch(getItemsAction());
    }, [dispatch]);

    useEffect(() => {
        getItems();
    }, [items, getItems]);

    return (
        <div className="wrapper-grid">
            <table>
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        {/*<th>Total Price</th>*/}
                    </tr>
                </thead>
                <tbody>
                {items && items.map((item, i) => (
                    <TableRow
                        key={`item-${i}`}
                        code={item.code}
                        itemName={item.itemName}
                        description={item.description}
                        unitPrice={item.unitPrice}
                        quantity={item.quantity}
                        totalPrice={item.totalPrice}
                        data-test='table-item'
                    />
                ))}
                </tbody>
            </table>
        </div>
    );
}

export default Table;