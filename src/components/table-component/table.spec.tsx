import React from 'react';
import * as reactRedux from 'react-redux'
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Table from "./index";

Enzyme.configure({ adapter: new Adapter() });

describe('Table tests', () => {
    const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

    const mockState = [
        {
            code: '001',
            itemName: 'Test Item 1',
            description: 'Test description',
            unitPrice: 3,
            quantity: 0,
            totalPrice: 0
        },
        {
            code: '002',
            itemName: 'Test Item 2',
            description: 'Test description',
            unitPrice: 7,
            quantity: 0,
            totalPrice: 0
        }
    ]

    beforeEach(() => {
        useSelectorMock.mockReturnValue(mockState);

        const mockDispatch = jest.fn();
        useDispatchMock.mockReturnValue(mockDispatch);
    })

    afterEach(() => {
        useSelectorMock.mockClear();
        useDispatchMock.mockClear();
    })

    it('Renders table with 2 rows', () => {
        let wrapper = shallow(<Table />);
        expect(wrapper.find('[data-test="table-item"]')).toHaveLength(2);
    })
})
