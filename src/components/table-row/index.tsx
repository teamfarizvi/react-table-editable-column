import React from 'react';
import { Item } from "../../app/models/item";
import {useDispatch} from "react-redux";
import {updateItemQuantity} from "../../app/redux/actions/itemActions";

const TableRow = (item: Item, index: number) => {

    const dispatch = useDispatch();

    const handleInputChange = (index: number, value: string) => {
        console.log(`Total Price: ${item.unitPrice * parseInt(value)}`);
        dispatch(updateItemQuantity(item.code, parseInt(value)));
    }

    return (
        <>
            <tr>
                <td className="table-cell">{item.code}</td>
                <td className="table-cell">{item.itemName}</td>
                <td className="table-cell">{item.description}</td>
                <td className="table-cell">{item.unitPrice}</td>
                <td className="table-cell">
                    <input
                        type="text"
                        onChange={e => handleInputChange(index, e.target.value)}
                        style={{width: '30px'}}
                    />
                </td>
                {/*<td className="table-cell">{item.totalPrice}</td>*/}
            </tr>
        </>
    )
}

export default TableRow;