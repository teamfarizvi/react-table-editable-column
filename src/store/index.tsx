import { applyMiddleware, combineReducers, createStore, Store } from "redux";
import thunk from "redux-thunk";
import { itemReducer } from "../app/redux/reducers/itemReducer";
import { ItemsState } from "../app/models/itemsState";

export interface IAppState {
    itemsState: ItemsState;
}

const rootReducer = combineReducers<IAppState>({
    itemsState: itemReducer,
});

export default function configureStore(): Store<IAppState, any> {
    const store = createStore(rootReducer, undefined, applyMiddleware(thunk));
    return store;
}