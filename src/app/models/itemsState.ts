import { Item } from "./item";

export interface ItemsState {
    items: Item[];
    loading: boolean;
    errorMessage: string;
    totalPrice: number;
}