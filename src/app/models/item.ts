export interface Item {
    code: string;
    itemName: string;
    description?: string;
    unitPrice: number;
    quantity: number;
    totalPrice: number;
    // handleInputChange: void;
}