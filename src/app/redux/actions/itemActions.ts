import { ActionCreator, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { ItemsState } from "../../models/itemsState";
import { ItemActionTypes } from "../types/itemActionTypes";
import {Item} from "../../models/item";
import axios from 'axios';

export interface IItemAction {
    type: ItemActionTypes.FETCH_ITEMS_SUCCESS,
    payload: Item[];
}

export interface ILoadItemAction {
    type: ItemActionTypes.FETCH_ITEMS_LOADING,
    loading: boolean;
}

export interface IErrorAction {
    type: ItemActionTypes.FETCH_ITEMS_ERROR,
    errorMessage: string;
}

export interface IUpdateItemQuantity {
    type: ItemActionTypes.UPDATE_ITEM_QUANTITY,
    payload: {
        code: string,
        quantity: number
    }
}

export type ItemActions = IItemAction | ILoadItemAction | IErrorAction | IUpdateItemQuantity;

export const getItemsAction: ActionCreator<ThunkAction<Promise<any>, ItemsState, null, IItemAction>> = () => {

    return async (dispatch: Dispatch) => {
        try {

            const result = await axios.get('https://api.mocki.io/v1/c9dcd7ce');

            dispatch({
                type: ItemActionTypes.FETCH_ITEMS_SUCCESS,
                payload: result.data.items
            });

        } catch (err) {
            console.error(err);

            dispatch({
                type: ItemActionTypes.FETCH_ITEMS_ERROR,
                errorMessage: err.message
            });

            dispatch({
                type: ItemActionTypes.FETCH_ITEMS_LOADING,
                loading: false
            });
        }
    }
}

export const loadItemsAction: ActionCreator<ThunkAction<any, ItemsState, null, ILoadItemAction>> = (shouldLoad: boolean) => (dispatch: Dispatch) => dispatch({
    type: ItemActionTypes.FETCH_ITEMS_LOADING,
    loading: shouldLoad
})

export const updateItemQuantity: ActionCreator<ThunkAction<any, ItemsState, null, ILoadItemAction>> = (code: string, quantity: number) => (dispatch: Dispatch) => dispatch({
    type: ItemActionTypes.UPDATE_ITEM_QUANTITY,
    payload: {
        code,
        quantity
    }
})