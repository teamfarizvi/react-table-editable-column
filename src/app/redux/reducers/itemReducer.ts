import { Reducer } from "redux";
import { ItemActionTypes } from "../types/itemActionTypes";
import { ItemsState } from "../../models/itemsState";
import { ItemActions } from "../actions/itemActions";

const initialState: ItemsState = {
    items: [],
    loading: false,
    errorMessage: '',
    totalPrice: 0
}

export const itemReducer: Reducer<ItemsState, ItemActions> = (state = initialState, action) => {
    switch (action.type) {
        case ItemActionTypes.FETCH_ITEMS_LOADING:
            return {
                ...state,
                loading: action.loading
            }

        case ItemActionTypes.FETCH_ITEMS_SUCCESS: {
            return {
                ...state,
                items: action.payload,
            }
        }

        case ItemActionTypes.FETCH_ITEMS_ERROR: {
            return {
                ...state,
                errorMessage: action.errorMessage,
                items: []
            }
        }

        case ItemActionTypes.UPDATE_ITEM_QUANTITY: {
            const itemIndex = state.items.findIndex(i => i.code === action.payload.code);

            const newItems = state.items.map((item, index) => {
                if (index !== itemIndex)
                    return item;

                return {
                    ...item,
                    totalPrice: item.unitPrice * action.payload.quantity,
                    quantity: action.payload.quantity
                }
            });

            const total = newItems
                            .filter(item => item.quantity > 0)
                            .reduce((sum: number, item) => item.totalPrice, 0);

            return {
                ...state,
                items: newItems,
                totalPrice: total
            }
        }

        default:
            return state;
    }
};