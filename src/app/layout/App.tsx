import React from 'react';
import './App.scss';
import { Homepage } from "../../pages";

function App() {
  return (
    <div className="wrapper-main">
      <Homepage />
    </div>
  );
}

export default App;
